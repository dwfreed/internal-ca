#!/bin/bash
read -ep "Where should we store the key? " PRIVKEY
echo "You're going to need some random for the key generation.  Background the script and install haveged or just run dd in a loop to generate some."
openssl genrsa -out "$PRIVKEY" -rand /dev/random 4096 # Because of apple, we can't have nice things (8192 bit keys)
