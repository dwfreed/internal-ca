#!/bin/bash
mkdir private certs crl
touch index.txt crlnumber
echo "01" > serial
chmod 700 private
echo "You're going to need some random for the key generation.  Background the script and install haveged or just run dd in a loop to generate some."
openssl genrsa -out private/cakey.pem -rand /dev/random 4096 # Because of apple, we can't have nice things (8192 bit keys)
echo "Now that the key generation is complete, bring the script back to the foreground and press enter to generate the CA certificate."
read
openssl req -new -x509 -key private/cakey.pem -out cacert.pem -config openssl.cnf -days 7305 -rand /dev/random
echo "All set.  Now you can distribute cacert.pem to everybody, and use the other scripts to start generating certificates."
