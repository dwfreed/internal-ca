#!/bin/bash
read -ep "Where can we find the CSR? " CSR
read -ep "Where should we store the cert? " CERT
openssl ca -config openssl.cnf -in "$CSR" -verbose -out "$CERT"
