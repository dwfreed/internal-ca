#!/bin/bash
read -ep "Where can we find the key? " PRIVKEY
read -ep "Where should we store the CSR? " CSR
openssl req -new -key "$PRIVKEY" -out "$CSR" -config openssl.cnf -days 365 -rand /dev/random
